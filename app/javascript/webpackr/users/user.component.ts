import { Component } from '@angular/core';
import { PostService } from '../services/post.service'
import templateString from './user.component.html'



@Component({
  selector: 'user',
  template: templateString,
  providers: [PostService]
})
export class UserComponent {
  name:string;
  email:string;
  address: address;
  hobbies: string[];
  posts: string[];
  showHobbies: boolean;

  constructor(private postService: PostService) {
    this.name = 'Hashmi Mohammed'
    this.email = 'hashmee.smf@gmail.com'
    this.address = {
        street: 'Sanathnagar',
        city: 'Hyderabad',
        state: 'Telangana'}
    this.hobbies = ['swimming', 'coding', 'roadtrip']
    this.showHobbies = false
    this.postService.getPosts().subscribe(posts => this.posts= posts)
  }

  toggleHobbies() {
    if(this.showHobbies == true){
      this.showHobbies = false;
    } else {
      this.showHobbies = true;
    }
  }

  addHobby(hobby) {
    this.hobbies.push(hobby)
  }
  deleteHobby(i) {
    this.hobbies.splice(i, 1);
  }

  
}
interface address {
  street: string;
  city: string;
  state: string;
    
}

interface posts {
  title: string;
  body: string;
}